@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">FORM CUSTOMER [ Modify ]</div>
                <form action="/customer/update/{{$data->id}}"  method="post" enctype="multipart/form-data">
                <div class="panel-body">
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Nama Lengkap :</label>
                      <input type="text" name="nama" id="nama" class="form-control" required="true" value="{{$data->nama}}">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Jenis Kelamin :</label>
                      <select name="jk" id="jk" class="form-control" required="true">
                        <option {{old('jk',$data->jk)=="Laki-laki"? 'selected':''}} >Laki-laki</option>
                        <option {{old('jk',$data->jk)=="Perempuan"? 'selected':''}} >Perempuan</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>No. Telpon :</label>
                      <input type="text" name="tlp" id="tlp" class="form-control" required="true" value="{{$data->tlp}}">
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Alamat :</label>
                      <textarea name="alamat" id="alamat" class="form-control" required="true">{{$data->alamat}}</textarea>
                    </div>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                      <label>Tempat :</label>
                      <input type="text" name="tempat" id="tempat" class="form-control" required="true" value="{{$data->tempat}}">
                    </div>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                      <label>Tanggal Lahir :</label>
                      <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datepicker" required="true" value="{{ \Carbon\Carbon::parse($data->tgl_lahir)->format('d/m/Y')}}">
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Foto :</label>
                      <input type="file" name="foto" id="foto" required="true">
                      <img id="img-upload" src="/images/foto/{{$data->foto}}" width="240" />
                    </div>
                  </div>
                </div>
                <div class="panel-body">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                  <button type="button" onclick="clearForm()" id="btn-clear" class="btn btn-info" style="display: none;"> Clear </button>
                  <a href="/customer" class="btn btn-danger">Cancel</a>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection
