@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 col-xs-12" id="hotel">
      <div class="row">
        <div class="col-md-12 col-xs-12 text-right">
         <button class="btn btn-primary" id="addnew"><span class="glyphicon glyphicon-plus"></span> tambah customer</button>
        </div>
        <div class="col-md-12 col-xs-12">
         <table class="table table-hover table-bordered nowrap" width="100%">
            <thead>
             <th>No</th>
             <th>Nama Lengkap</th>
             <th>Jenis Kelamin</th>
             <th>No.Telp</th>
             <th>Alamat</th>
             <th>Tempat</th>
             <th>Tanggal Lahir</th>
             <th>Foto</th>
             <th width="100">Tindakan</th>
            </thead>
            <tbody>
              <?php $i = 0; ?>
              @foreach($customer as $data)
                <?php $i++; ?>
                <td>{{$i}}</td>
                <td>{{$data->nama}}</td>
                <td>{{$data->jk}}</td>
                <td>{{$data->tlp}}</td>
                <td>{{$data->alamat}}</td>
                <td>{{$data->tempat}}</td>
                <td>{{$data->tgl_lahir}}</td>
                <td><center><img src="images/foto/{{$data->foto}}" width="120"></center></td>
                <td width="100">
                  <!-- <button id="editdata" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></button> -->
                  <a class="btn btn-warning" href="/customer/edit/{{$data->id}}" ><span class="glyphicon glyphicon-pencil"></span></a>
                  <a class="btn btn-danger" href="/customer/delete/{{$data->id}}" onclick="return confirm('{{$data->nama}} akan dihapus?')"><span class="glyphicon glyphicon-trash"></span></a>

                </td>
              @endforeach
            </tbody>
         </table>
        </div>
      </div>
    </div>

    <button type="button" class="btn btn-primary" style="display: none;" id="btnModal" data-toggle="modal" data-target="#exampleModalLong">
    Launch demo modal
    </button>

      <!-- Insert -->
      <form action="/customer/insert"  method="post" enctype="multipart/form-data">
      <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #043279;color: #fff;">
              <h5 class="modal-title" id="exampleModalLongTitle">FORM CUSTOMER [ ADD NEW ]</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label>Nama Lengkap :</label>
                    <input type="text" name="nama" id="nama" class="form-control" required="true">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label>Jenis Kelamin :</label>
                    <select name="jk" id="jk" class="form-control" required="true">
                      <option>Laki-laki</option>
                      <option>Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label>No. Telpon :</label>
                    <input type="text" name="tlp" id="tlp" class="form-control" required="true">
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label>Alamat :</label>
                    <textarea name="alamat" id="alamat" class="form-control" required="true"></textarea>
                  </div>
                </div>
                <div class="col-md-3 col-xs-12">
                  <div class="form-group">
                    <label>Tempat :</label>
                    <input type="text" name="tempat" id="tempat" class="form-control" required="true">
                  </div>
                </div>
                <div class="col-md-3 col-xs-12">
                  <div class="form-group">
                    <label>Tanggal Lahir :</label>
                    <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datepicker" required="true">
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label>Foto :</label>
                    <input type="file" name="foto" id="foto" required="true">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
               <button type="button" onclick="clearForm()" id="btn-clear" class="btn btn-info" style="display: none;"><i class="fa fa-refresh" ></i> Clear </button>
               <button type="button" id="btn-close" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
            </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

