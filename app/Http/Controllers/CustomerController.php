<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::all();
        return view('customer_management', compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $customer = new Customer();
            $path = public_path('images/foto');
            $filename = $request->nama.'_'.date('ymdhis').'.jpg';
            $request->file('foto')->move($path, $filename);
            $customer->nama = $request->nama;
            $customer->jk = $request->jk;
            $customer->tlp = $request->tlp;
            $customer->alamat = $request->alamat;
            $customer->tempat = $request->tempat;
            $customer->tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
            $customer->foto = $filename;
            $customer->save();

            return redirect('/customer');

        } catch(Exception $e){
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Customer::where('id', $id)->first();

        return view('customer_edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $customer = Customer::find($id);
            $path = public_path('images/foto');
            $filename = $request->nama.'_'.date('ymdhis').'.jpg';
            $request->file('foto')->move($path, $filename);
            $customer->nama = $request->nama;
            $customer->jk = $request->jk;
            $customer->tlp = $request->tlp;
            $customer->alamat = $request->alamat;
            $customer->tempat = $request->tempat;
            $customer->tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
            $customer->foto = $filename;
            $customer->update();

            return redirect('/customer');

        } catch(Exception $e){
            return $e;
        }
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Customer::find($id);

        $data->delete();

        return redirect()->to('/customer');
    }
}
